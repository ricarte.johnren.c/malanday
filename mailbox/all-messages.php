<?php require_once '../connection/connect.php'; ?>
<?php
// Get all messages from inbox table
$inbox = "SELECT * FROM inbox WHERE active = 1 ORDER BY id DESC ";
$result = $conn->query($inbox);

// Count all rows from inbox table
$count = "SELECT count(*) as message_count FROM inbox";
$result_count = $conn->query($count);

while ($row = $result_count->fetch_assoc()) {
    $message_count = $row['message_count'];
}

$latest_message = "SELECT * FROM inbox ORDER BY id DESC LIMIT 3";
$notification_message = $conn->query($latest_message);
?>