<?php 
$select_reservation = "SELECT * FROM reservation";
$result_reservation = $conn->query($select_reservation);
?>
<br>
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
		
		<div class="card col-sm-12">
		
              <div class="card-header">
                <h3 class="card-title">Reservation List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Reference #</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th style="text-align:center">Check In</th>
				          	<th style="text-align:center">Check Out</th>
				          	<th style="text-align:center">Schedule</th>
                    <th style="text-align:center">Status</th>
                    <th style="text-align:center">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
				  while($row_reservation = $result_reservation->fetch_assoc())
				  {
				  ?>
				  <tr>
                    <td><?php echo $row_reservation['reference_no'];?></td>
                    <td><?php echo $row_reservation['Name'];?></td>
                    <td><?php echo $row_reservation['ContactNumber'];?></td>
                    <td style="text-align:center"><?php echo date_format(date_create($row_reservation['Checkindate']),'F j, Y')?></td>
                    <td style="text-align:center"><?php echo date_format(date_create($row_reservation['Checkoutdate']),'F j, Y')?></td>
                    <td style="text-align:center"><?php echo $row_reservation['Schedule'];?></td>
                    <td style="text-align:center">
                    <?php  if($row_reservation['status']=="Reserved"){?>
                      <span class="badge badge-success">Reserved</span>
                    <?php  }?>
                    <?php  if($row_reservation['status']=="Pending"){?>
                      <span class="badge badge-warning text-white">Pending</span>
                    <?php  }?>
                    <?php  if($row_reservation['status']=="Cancelled"){?>
                      <span class="badge badge-danger">Cancelled</span>
                    <?php  }?>
                    <?php  if($row_reservation['status']=="Completed"){?>
                      <span class="badge badge-success">Completed</span>
                    <?php  }?>
                    </td>
				        	  <td>
                    <button type="button" class="btn btn-block btn-default btn-xs" onClick="viewDetails(<?php echo $row_reservation['reference_no'];?>)">View Details</button>
                    </td>
                  </tr>
                  <?php
				  }
				  ?>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>

        </div>
    </div>
    </div>
</section>
<!-- Modal -->
<div id="viewDetails" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="modalHeader"></h4>
            </div>
            <div class="modal-body">
              <h5>CUSTOMER INFORMATION</h5>
              <table class="table">
                <tbody>
                  <tr>
                    <th style="width:300px">Full Name:</th>
                    <td id="setName"></td>
                  </tr>
                  <tr>
                    <th>Email:</th>
                    <td id="setEmail"></td>
                  </tr>
                  <tr>
                    <th>Contact Number:</th>
                    <td id="setContact"></td>
                  </tr>
                  <tr>
                    <th>Address:</th>
                    <td id="setAddress"></td>
                  </tr>
                </tbody>
              </table>
              <hr>
              <h5>RESERVATION DETAILS</h5>
              <table class="table">
                <tbody>
                  <tr>
                    <th style="width:300px">Check-in Date</th>
                    <td id="setCheckin"></td>
                  </tr>
                  <tr>
                    <th>Status:</th>
                    <td id="setStatus"></td>
                  </tr>
                  <tr>
                    <th>Check-out Date:</th>
                    <td id="setCheckout"></td>
                  </tr>
                  <tr>
                    <th>Schedule:</th>
                    <td id="setSchedule"></td>
                  </tr>
                  <tr>
                    <th>Pax:</th>
                    <td id="setPax"></td>
                  </tr>
                  <tr>
                    <th>Number of Adults:</th>
                    <td id="setAdultCount"></td>
                  </tr>
                  <tr>
                    <th>Number of Children:</th>
                    <td id="setChildrenCount"></td>
                  </tr>
                </tbody>
              </table>
              <h5>PRICING</h5>
              <table class="table">
                <tbody>
                  <tr >
                    <th style="width:300px">Resort Fee:</th>
                    <td id="setResortFee"></td>
                  </tr>
                  <tr>
                    <th>Reservation Fee:</th>
                    <td id="setResFee"></td>
                  </tr>
                  <tr>
                    <th>Convenience Fee:</th>
                    <td id="setConFee"></td>
                  </tr>
                  <tr>
                    <th>Balance:</th>
                    <td id="setBalance"></td>
                  </tr>
                  <tr>
                    <td><b>Total Fee</b> (Resort Fee + Convenience Fee):</td>
                    <th id="setTotalFee"></th>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="cancelBtn" class="btn btn-danger" data-toggle="modal" data-target="#confirmCancelModal">Cancel</button>
              <button type="button" id="completeBtn" class="btn btn-success" data-toggle="modal" data-target="#markCompleteModal">Complete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>

  </div>
</div>

<!-- Confirm Cancellation Modal -->
<div id="confirmCancelModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">
              <span class="fa fa-exclamation-circle info"></span> Confirm Reservation</h5>
            </div>
            <form method="post" action='../query.php'>
              <div class="modal-body">
                <p>This operation is irreversible, are you sure you want to cancel this reservation?</p>
              </div>
              <input type="hidden" id="refno" name="refno">
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" value="submit" name="cancelReservation" class="btn btn-danger">Cancel Reservation</button>
              </div>
            </form>
          </div>
    </div>
</div>

<!-- Confirm Completion Modal -->
<div id="markCompleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">
              <span class="fa fa-exclamation-circle info"></span> Mark as Complete</h5>
            </div>
            <form method="post" action='../query.php'>
              <div class="modal-body">
                <p>Marking reservation as complete means that you have received the remaining balance, do you want to continue?</p>
              </div>
              <input type="hidden" id="refnoComplete" name="refno">
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" value="submit" name="completeReservation" class="btn btn-success">Complete Reservation</button>
              </div>
            </form>
          </div>
    </div>
</div>
<script>
  function viewDetails(refNo){
    $('#modalHeader').html("Reservation Details ("+refNo+")");
    var resDetails = [];
    getRequestDetails(refNo);
    $('#viewDetails').modal('show');
  }

  function getRequestDetails(refNo){
    //Get reservation details via ajax request
    
    const xmlhttp = new XMLHttpRequest();

    xmlhttp.onload = function() {
      const res = JSON.parse(this.responseText);
      // document.getElementById("demo").innerHTML = res.name;
      // console.log(res.name);
      $('#setName').html(res.name);
      $('#setEmail').html(res.email);
      $('#setContact').html(res.contact);
      $('#setAddress').html(res.address);
      $('#setCheckin').html(res.checkin);
      $('#setCheckout').html(res.checkout);
      $('#setSchedule').html(res.schedule);
      $('#setPax').html(res.pax);
      $('#setAdultCount').html(res.adult);
      $('#setChildrenCount').html(res.child);
      $('#setResortFee').html(res.resortFee);
      $('#setResFee').html(res.resFee);
      $('#setConFee').html(res.conFee);
      $('#setBalance').html(res.balance);
      $('#setStatus').html(res.status);
      $('#setTotalFee').html(res.ttlFee);
      $('#refno').val(res.refno);
      $('#refnoComplete').val(res.refno);

      if(res.status == "Cancelled" || res.status == "Completed"){
        $('#cancelBtn').hide();
        $('#completeBtn').hide();
      }
      if(res.status == "Pending"){
        $('#completeBtn').hide();
        $('#cancelBtn').show();
      }
      if(res.status == "Reserved"){
        $('#cancelBtn').show();
        $('#completeBtn').show();
      }
    }
    xmlhttp.open("GET", "../getReservationDetails.php?refno=" + refNo);
    xmlhttp.send();
  }
</script>