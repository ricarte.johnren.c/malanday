<?php
require_once '..\vendor\phpmailer\phpmailer\src\Exception.php';
require_once '..\vendor\phpmailer\phpmailer\src\PHPMailer.php';
require_once '..\vendor\phpmailer\phpmailer\src\SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

date_default_timezone_set("Asia/Manila"); //Timezone


function sendMail($refno, $email) {
    // passing true in constructor enables exceptions in PHPMailer
    $mail = new PHPMailer(true);
    $setDate = date("F j, Y h:i A");
    try {

        $body = file_get_contents("..\admin\invoice\invoice_mail.php");
        $body = str_replace('$refno', $refno, $body);
        $body = str_replace('$date', $setDate, $body);

        // Server settings
        $mail->SMTPDebug = 0; // for detailed debug output
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port = 587;

        $mail->Username = 'malandaymercedes@gmail.com'; // YOUR gmail email
        $mail->Password = 'malandaymercedes10010'; // YOUR gmail password

        // Sender and recipient settings
        $mail->setFrom('malandaymercedes@gmail.com', 'Malanday Resort Reservation');
        $mail->addAddress($email, 'Customer');
        // $mail->addReplyTo('example@gmail.com', 'Sender Name'); // to set the reply to

        // Setting the email content
        $mail->IsHTML(true);
        $mail->Subject = "Your reservation has been booked!";
        $mail->Body = $body;
        $mail->AltBody = 'Plain text message body for non-HTML email client. Gmail SMTP email body.';

        $mail->send();
        echo "Email message sent.";
    } catch (Exception $e) {
        echo "Error in sending email. Mailer Error: {$mail->ErrorInfo}";
    }

    header ("Location: ../reservation.php?reservation=success&ref=".$refno);

}

?>
