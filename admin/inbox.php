<?php
include '../admin/script.php';
include '../mailbox/all-messages.php';
?>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" id="delete_id" name="delete-id">
      <div class="modal-body">
        You want to delete this message?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" id="yes-btn">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="card col-sm-12">

        <div class="card-header">
          <h3 class="card-title">Inbox</h3>
        </div>
        <!-- /.card-header -->

        <div class="card-body">
          <table id="example" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Action</th>
                <th class="all">Email</th>
                <th class="all">Message</th>
                <th class="all">Date</th>
                <th class="all"></th>

                <th class="none">Full Name: </th>
                <th class="none">Message: </th>
              </tr>
            </thead>
            <tbody>
              <?php
              while ($row = $result->fetch_assoc()) {
                $date =  date_create($row['createdDate']);
                $formated_date = date_format($date, "F j, Y, g:i a");
              ?>
              <tr>
                <td></td>
                <td><a href="https://mail.google.com/mail/u/0/?pli=1#inbox?compose=new"><?php echo $row['email'] ?></a>
                </td>
                <td><?php echo $row['message'] ?></td>
                <td><?php echo $formated_date ?></td>

                <td><button class="btn btn-danger" id="delete-message" data-toggle="modal" data-target="#exampleModal"
                    data-a="<?php echo $row['id'] ?>"><i class="fas fa-trash"></i></button></td>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['message'] ?></td>
              </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
var table = $('#example').DataTable({
  responsive: {
    details: {
      type: 'column'
    }
  },
  columnDefs: [{
    className: 'control',
    orderable: false,
    targets: 0
  }],
  order: [0, 'desc']
});

$('#example tbody').on('click', '#delete-message', function() {
  var messageId = Number($(this).attr("data-a"));
  $('#delete_id').val(messageId);
});

$('#yes-btn').on('click', (e) => {
  var messageId = $('#delete_id').val();
  $.ajax({
    url: "../mailbox/hide-message.php",
    method: "POST",
    data: {
      messageId: messageId
    },
    success: function(data) {
      $('#exampleModal').modal('hide');
      location.reload();
    }
  })
})
</script>