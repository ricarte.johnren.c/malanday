<?php
header('Content-Type: text/html; charset=ISO-8859-1');
?>
<?php require_once '../connection/connect.php'; ?>
<?php require_once './logcheck.php'; ?>
<!DOCTYPE html>
<html>
<?php require_once './head.php'; ?>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <?php require_once 'navbar.php'; ?>
    <?php require_once 'sidebar.php'; ?>
    <div class="content-wrapper">
      <?php
      if (isset($_GET['inbox'])) {
        include './inbox.php';
      } else if (isset($_GET['reservation'])) {
        include './reservation.php';
      } else {
        include './dashboard.php';
      }

      ?>

    </div>
    <?php require_once 'footer.php'; ?>
  </div>
</body>

</html>