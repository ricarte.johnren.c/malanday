<?php require_once '../connection/connect.php'; ?>

<?php
//Current Sales for the inputed month
function getAllPricesPerMonth($month, $year, $conn)
{
  $checkout_date = "SELECT sum(price) as totalPrice from reservation where MONTH(`Checkoutdate`) = $month and YEAR(`Checkoutdate`) = $year";
  $checkout_date_count = $conn->query($checkout_date);
  $fetch_total_price = mysqli_fetch_assoc($checkout_date_count);
  return $fetch_total_price['totalPrice'];
}

//Current Sales per year
function getAllPricesPerYear($year, $conn)
{
  $array_prices = [];
  for ($i = 1; $i <= 12; $i++) {
    array_push($array_prices, getAllPricesPerMonth($i, $year, $conn));
  }

  return $array_prices;
}

function getTotalRevenuePerYear($year, $conn)
{
  $revenue_year = "SELECT sum(price) as totalRevenue from reservation where YEAR(`Checkoutdate`) = $year";
  $revenue_year_count = $conn->query($revenue_year);
  $fetch_revenue_year = mysqli_fetch_assoc($revenue_year_count);
  return $fetch_revenue_year['totalRevenue'];
}
$revenue_per_year = getTotalRevenuePerYear(date("Y"), $conn);
$current_sales_per_year = getAllPricesPerYear(date("Y"), $conn);
$previous_sales_per_year = getAllPricesPerYear(date("Y", strtotime("-1 year")), $conn);

?>