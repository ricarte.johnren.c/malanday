<?php require_once '../connection/connect.php'; ?>

<?php
function getBookingStatus($book_status, $conn)
{
  $booking_status = "SELECT count(*) as bookstatus FROM reservation WHERE status = '$book_status'";
  $result_count = $conn->query($booking_status);
  $booking_row = mysqli_fetch_assoc($result_count);
  return $booking_row['bookstatus'];
}
// Get all cancelled booking
$cancelled_booking_count = getBookingStatus('Cancelled', $conn);
// Get all new bookings and reserved visitors
$reserved_booking_count = getBookingStatus('Reserved', $conn);

function getAllVisitors($week, $conn)
{
  if ($week) {
    $incoming_visitors = "SELECT sum(Adults) as adults, sum(Children) as children FROM reservation WHERE WEEK(`Checkindate`) = $week";
  } else {
    $incoming_visitors = "SELECT sum(Adults) as adults, sum(Children) as children FROM reservation";
  }

  $incoming_visitors_count = $conn->query($incoming_visitors);
  $incoming_visitors_row = mysqli_fetch_assoc($incoming_visitors_count);
  $incoming_adult = $incoming_visitors_row['adults'];
  $incoming_children = $incoming_visitors_row['children'];
  return $incoming_adult + $incoming_children;
}

// Get all check-in visitors
$total_incoming_visitors = getAllVisitors(null, $conn);
// Get all check-out visitors
$total_outgoing_visitors = getAllVisitors(null, $conn);

$total_visitors_this_week = getAllVisitors(date("W"), $conn);

function getAllVisitorsPerDay($day, $week, $conn)
{
  $visitors = "SELECT sum(Adults) as adults, sum(Children) as children FROM reservation where WEEK(`Checkindate`) = $week and DAYOFWEEK(`Checkindate`) = $day";
  $visitors_count = $conn->query($visitors);
  $visitors_row = mysqli_fetch_assoc($visitors_count);

  $adults = $visitors_row['adults'];
  $children = $visitors_row['children'];
  return $adults + $children;
}

//Get all visitors per week
function getAllVisitorsPerWeek($week, $conn)
{
  $array_prices = [];
  for ($i = 1; $i <= 7; $i++) {
    array_push($array_prices, getAllVisitorsPerDay($i, $week, $conn));
  }

  return $array_prices;
}

$visitors_this_week = getAllVisitorsPerWeek(date("W"), $conn);
$visitors_last_week = getAllVisitorsPerWeek(date("W", strtotime("-1 week")), $conn);

?>