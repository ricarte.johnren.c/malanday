  <?php
    include '../mailbox/all-messages.php';
    ?>
  <nav class="main-header navbar navbar-expand navbar-info navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->


      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-envelope"></i>
          <span class="badge badge-danger navbar-badge"><?php echo $message_count ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <?php
                    while ($row = $notification_message->fetch_assoc()) {
                        $date =  date_create($row['createdDate']);
                        $formated_date = date_format($date, "F j, Y - g:i a");
                    ?>
          <a href="index.php?inbox" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="../dist/img/default.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  <b><?php echo $row['name'] ?></b>
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm text-muted">
                  <i class="far fa-clock mr-1"></i>
                  <?php echo $formated_date ?>
                </p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <?php } ?>
          <div class="dropdown-divider"></div>
          <a href="index.php?inbox" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>


    </ul>
  </nav>