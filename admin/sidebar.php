 <aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="#" class="brand-link">
     <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
     <span class="brand-text font-weight-light">Malanday | Resort</span>
   </a>

   <!-- Sidebar -->
   <div class="sidebar">
     <!-- Sidebar user panel (optional) -->
     <div class="user-panel mt-3 pb-3 mb-3 d-flex">
       <div class="image">
         <img src="../dist/img/default.png" class="img-circle elevation-2" alt="User Image">
       </div>
       <div class="info">
         <a href="#" class="d-block">Admin</a>
       </div>
     </div>




     <!-- Sidebar Menu -->
     <nav class="mt-2">
       <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

         <li class="nav-item">
           <a href="index.php" class="nav-link side-dashboard active">
             <i class="fas fa-chart-line nav-icon"></i>
             <p>
               Dashboard
             </p>
           </a>
         </li>

         <li class="nav-item">
           <a href="?reservation" class="nav-link side-reservation">
             <i class="fas fa-book nav-icon"></i>
             <p>
               Reservation

             </p>
           </a>
         </li>

         <li class="nav-item">
           <a href="?inbox" class="nav-link side-inbox">
             <i class="fas fa-envelope nav-icon"></i>
             <p>
               Inbox

             </p>
           </a>
         </li>

         <li class="nav-item">
           <a href="logout.php" class="nav-link">
             <i class="fas fa-power-off nav-icon"></i>
             <p>
               Logout

             </p>
           </a>
         </li>

       </ul>
     </nav>
     <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->

   <script>
     $(document).ready(function() {
       var pathname = window.location.href;
       console.log("🚀 ~ file: sidebar.php ~ line 77 ~ $ ~ pathname", pathname)
       if (pathname.includes('inbox')) {
         $(".side-inbox").addClass('active');

         $(".side-dashboard").removeClass('active');
       }

       if (pathname.includes('reservation')) {
         $(".side-reservation").addClass('active');

         $(".side-inbox").removeClass('active');
         $(".side-dashboard").removeClass('active');
       }
     });
   </script>
 </aside>