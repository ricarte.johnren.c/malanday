<?php
require_once 'connection/connect.php';
date_default_timezone_set("Asia/Manila"); //Timezone
?>

<?php
if (isset($_POST['submit'])) {
    //Set variable from  post request
    $name = $_POST['Name'];
    $contact = $_POST['ContactNumber'];
    $add = $_POST['Address'];
    $mail = $_POST['Email'];
    $adlts = $_POST['Adults'];
    $child = $_POST['Children'];
    $checkin = $_POST['Checkindate'];
    $checkout = $_POST['Checkoutdate'];
    $sched = $_POST['SwimHour'];
    $pax = $_POST['formpax'];
    $price = $_POST['price'];
    $status = $_POST['status'];
    $refno = time(); //timestamp for reference number

    //Checking availabilty of the schedules
    $chckAvailabilty = mysqli_query($conn, "SELECT * FROM `schedules` WHERE `date` BETWEEN '$checkin' AND '$checkout' AND `schedule` = '$sched'");
    $availCount = mysqli_num_rows($chckAvailabilty);
    if ($availCount > 0) {
        header("Location: public/reservation.php?reservation=error&msg=selected dates are not available. Please see calendar for the available schedules");
        exit();
    }

    //Inserting records to reservation table
    $query = "INSERT INTO `reservation` (`ID`, `Name`, `ContactNumber`, `Address`, `Email`, `Adults`, `Children`, `Checkindate`, `Checkoutdate`, `Schedule`, `Pax`, `status`, `price`, `reference_no`) 
	VALUES (NULL, '{$name}', '{$contact}', '{$add}', '{$mail}', '{$adlts}', '{$child}', '{$checkin}', '{$checkout}', '{$sched}', '{$pax}', '{$status}', '{$price}', '{$refno}');";
    if ($result = $conn->query($query)) {
        //Get ID
        $rQuery = mysqli_query($conn, "SELECT * FROM `reservation` WHERE `reference_no`=" . $refno);
        $reservation = mysqli_fetch_array($rQuery);

        //Save schedules
        if ($checkin != $checkout) {
            $dates = getDates($checkin, $checkout);
            foreach ($dates as $key => $value) {
                $scheduleQuery = "INSERT INTO `schedules` VALUES (NULL, '{$reservation['ID']}', '{$value->format('Y-m-d')}', '{$sched}')";
                $insertSchedules = $conn->query($scheduleQuery);
            }
        } else {
            $scheduleQuery = "INSERT INTO `schedules` VALUES (NULL, '{$reservation['ID']}', '{$checkin}', '{$sched}')";
            $insertSchedules = $conn->query($scheduleQuery);
        }
    }
    $conn->close();
    $_SESSION['res_refno'] = $refno;
    $_SESSION['res_email'] = $mail;
	header ("Location: payment/create-source.php");

}

function getDates($start, $end)
{
    //For getting the dates between the start date and end date
    $period = new DatePeriod(
        new DateTime($start),
        new DateInterval('P1D'),
        new DateTime(date('y-m-d', strtotime($end . ' +1 day')))
    );

    return $period;
}

if (isset($_POST['cancelReservation'])) {

    if(verifyReference($_POST['refno'], $conn)){    
        $query = "UPDATE `reservation` SET `status` = 'Cancelled' WHERE `reservation`.`reference_no` =".$_POST['refno'];
        $runQuery = mysqli_query($conn, $query);
        
        $conn->close();
        header("Location: admin/index.php?reservation");
    }else{
        header("Location: admin/index.php?reservation");
    }
}

if (isset($_POST['completeReservation'])) {

    if(verifyReference($_POST['refno'], $conn)){
        $query = "UPDATE `reservation` SET `status` = 'Completed' WHERE `reservation`.`reference_no` =".$_POST['refno'];
        $runQuery = mysqli_query($conn, $query);
        
        $conn->close();
        header("Location: admin/index.php?reservation");
    }else{
        header("Location: admin/index.php?reservation");
    }
}

function verifyReference($refNo, $conn){
    $getDetails = mysqli_query($conn, "SELECT * FROM `reservation` WHERE `reference_no` = '$refNo' AND (`status`='Reserved' OR `status`='Pending')");
    $count = mysqli_num_rows($getDetails);

    if($count > 0){
        return true;
    }else{
        return false;
    }
}
?>
