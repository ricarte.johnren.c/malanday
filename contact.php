<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include "./lib/head.php"; ?>

<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">
      <?php include "./lib/navbar.php" ?>
      <!-- end:fh5co-header -->
      <div class="fh5co-parallax" style="background-image: url(images/slider1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
              <div class="fh5co-intro fh5co-table-cell">
                <h1 class="text-center">Contact Us</h1>
                <p>We love questions and feedbacks - and we're always happy to help!</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="fh5co-contact-section">
        <div class="row">
          <div class="col-md-6">
            <div class="fh5co-map">
              <iframe width="900" height="700" id="map" src="https://maps.google.com/maps?q=Malanday%20Mercedes%20Spring&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="#"></a><br>
            </div>
          </div>
          <div class="col-md-6">
            <!-- <div class="col-md-12"> -->
            <h3>Our Address</h3>
            <p class="card-text">Here are some ways to contact us: </p>
            <ul class="contact-info">
              <li><i class="ti-map"></i>Barangay Perez, Calauan, Laguna</li>
              <li><i class="ti-mobile"></i>+63 947-429-6376</li>
              <li><i class="ti-home"></i><a href="https://www.facebook.com/Malanday-mercedes-spring-576510512756377">Malanday Mercedes
                  Spring</a></li>
            </ul>
            <!-- </div> -->
            <form action="./mailbox/send-message.php" method="POST">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Name" name="name" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email" name="email" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" id="" cols="30" rows="7" placeholder="Message" name="message" required></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="submit" value="Send Message" class="btn btn-success" name="submit">
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>


      <?php include "./lib/footer.php"; ?>
    </div>
    <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->

  <?php include "./lib/script.php"; ?>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxe0wEZmT6Tivj8TrjxqYv50u9jSTBORo&callback=initMap&libraries=&v=weekly">
  </script>
  <script src="js/google_map.js"></script>

</body>

</html>
<script src="../plugins/jquery/jquery.min.js"></script>
<script>
  $(function() {
    var pathname = window.location.pathname;
    if (pathname.includes('contact')) {
      $(".nav-contact").addClass('active');

      $(".nav-homepage").removeClass('active');
      $(".nav-gallery").removeClass('active');
      $(".nav-about").removeClass('active');
      $(".nav-reservation").removeClass('active');
    }

    $(document).ready(function() {
      var url = new URL(window.location.href);
      var status = url.searchParams.get("success");

      if (status === 'true') {
        alert('Message sent!')
      } else {
        $('.alert').hide();
      }
    });
  })
</script>