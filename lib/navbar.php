<div id="fh5co-header">
  <header id="fh5co-header-section">
    <div class="container">
      <div class="nav-header">
        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
        <h1 id="fh5co-logo"><a href="index.php">Malanday | Resort</a></h1>
        <nav id="fh5co-menu-wrap" role="navigation">
          <ul class="sf-menu" id="fh5co-primary-menu">
            <li><a class="nav-homepage active" href="index.php">Home</a></li>
            <li>
              <a class="nav-gallery" href="gallery.php">Gallery</a>
            </li>
            <li><a class="nav-reservation" href="reservation.php">Reservation</a></li>
            <li><a class="nav-about" href="about.php">About Us</a></li>
            <li><a class="nav-contact" href="contact.php">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
</div>