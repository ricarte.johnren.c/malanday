<footer id="footer" class="fh5co-bg-color">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="copyright">
          <p>&copy; 2021 Malanday Mercedes Resort <br> All Rights Reserved. <br>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="social-icons">
          <li>
            <a href="#"><i class="icon-twitter-with-circle"></i></a>
            <a href="#"><i class="icon-facebook-with-circle"></i></a>
            <a href="#"><i class="icon-instagram-with-circle"></i></a>
            <a href="#"><i class="icon-linkedin-with-circle"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>