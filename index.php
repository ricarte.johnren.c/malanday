<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<?php include "./lib/head.php"; ?>

<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">
      <?php include "./lib/navbar.php"; ?>
      <!-- end:fh5co-header -->
      <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
          <ul class="slides">
            <li style="background-image: url(images/image-2.jpeg);">
              <div class="overlay-gradient"></div>
              <div class="container">
                <div class="col-md-12 col-md-offset-0 text-center slider-text">
                  <div class="slider-text-inner js-fullheight">
                    <div class="desc">
                      <p><span>Comfortable Place</span></p>
                      <h2>A Best Place To Enjoy Your Life</h2>
                      <p>
                        <a href="reservation.php" class="btn btn-success btn-lg">Book Now</a>
                        <a href="#fh5co-counter-section" class="btn btn-info btn-lg">Check Availability</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li style="background-image: url(images/cottages1.jpeg);">
              <div class="overlay-gradient"></div>
              <div class="container">
                <div class="col-md-12 col-md-offset-0 text-center slider-text">
                  <div class="slider-text-inner js-fullheight">
                    <div class="desc">
                      <p><span>Relaxing view</span></p>
                      <h2>Make Your Vacation Comfortable</h2>
                      <p>
                        <a href="reservation.php" class="btn btn-success btn-lg">Book Now</a>
                        <a href="#fh5co-counter-section" class="btn btn-info btn-lg">Check Availability</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li style="background-image: url(images/room1.jpeg);">
              <div class="overlay-gradient"></div>
              <div class="container">
                <div class="col-md-12 col-md-offset-0 text-center slider-text">
                  <div class="slider-text-inner js-fullheight">
                    <div class="desc">
                      <p><span>Comfortable Place</span></p>
                      <h2>A Best Place To Enjoy Your Life</h2>
                      <p>
                        <a href="reservation.php" class="btn btn-success btn-lg">Book Now</a>
                        <a href="#fh5co-counter-section" class="btn btn-info btn-lg">Check Availability</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </li>

          </ul>
        </div>
      </aside>
      <div class="wrap">
        <div class="container">
          <div class="row">
          </div>
        </div>
      </div>

      <!-- <div id="fh5co-counter-section" class="fh5co-counters">
        <div class="container">
          <div class="row">
            <div class="col-md-4 text-center">
              <span class="fh5co-counter js-counter" data-from="0" data-to="20356" data-speed="5000"
                data-refresh-interval="50"></span>
              <span class="fh5co-counter-label">User Access</span>
            </div>
            <div class="col-md-4 text-center">
              <span class="fh5co-counter js-counter" data-from="0" data-to="8200" data-speed="5000"
                data-refresh-interval="50"></span>
              <span class="fh5co-counter-label">Transactions</span>
            </div>
            <div class="col-md-4 text-center">
              <span class="fh5co-counter js-counter" data-from="0" data-to="8763" data-speed="5000"
                data-refresh-interval="50"></span>
              <span class="fh5co-counter-label">Rating &amp; Review</span>
            </div>
          </div>
        </div>
      </div> -->

      <div id="fh5co-counter-section" class="fh5co-counters">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="section-title text-center">
                <!-- <h2>Availability</h2> -->
              </div>
            </div>
          </div>
          <?php include "./availability.php"; ?>
        </div>
      </div>

      <div id="featured-hotel" class="fh5co-bg-color">
        <div class="container">

          <div class="row">
            <div class="col-md-12">
              <div class="section-title text-center">
                <h2>Our Facilities</h2>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="feature-full-1col">
              <div class="image" style="background-image: url(images/image21.jpg);">
                <div class="descrip text-center">
                  <p><small>For as low as</small><span>PHP4000/day</span></p>
                </div>
              </div>
              <div class="desc">
                <h3>Attic</h3>
                <p>To give a good services and comfortable vacation. Recommendations is in favor to what people
                  expected to the resort.</p>
                <p><a href="reservation.php" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a></p>
              </div>
            </div>

            <div class="feature-full-2col">
              <div class="f-hotel">
                <div class="image" style="background-image: url(images/image2.jpg);">
                  <div class="descrip text-center">
                    <p><small>For as low as</small><span>PHP4000/day</span></p>
                  </div>
                </div>
                <div class="desc">
                  <h3>Spring</h3>
                  <p>To provide a quality and affordable spring resorts. Allow guest to experience the consistency
                    of relaxation that suits for them to unwind.</p>
                  <p><a href="reservation.php" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a>
                  </p>
                </div>
              </div>
              <div class="f-hotel">
                <div class="image" style="background-image: url(images/image9.jpg);">
                  <div class="descrip text-center">
                    <p><small>For as low as</small><span>PHP4000/day</span></p>
                  </div>
                </div>
                <div class="desc">
                  <h3>Clean Water</h3>
                  <p>Bring low end offers which guests can experience the
                    joy and hospitality of Malanday Mercedes Spring Resort.</p>
                  <p><a href="reservation.php" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a>
                  </p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div id="hotel-facilities">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="section-title text-center">
                <h2>About Us</h2>
              </div>
            </div>
          </div>

          <div id="tabs">
            <nav class="tabs-nav">
              <a href="#" class="active" data-tab="tab1">
                <span>Mission</span>
              </a>
              <a href="#" data-tab="tab2">
                <span>Vision</span>
              </a>
              <a href="#" data-tab="tab3">
                <span>History</span>
              </a>
            </nav>
            <div class="tab-content-container">
              <div class="tab-content active show" data-tab-content="tab1">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6">
                      <img src="images/image1.jpg" class="img-responsive" alt="Image">
                    </div>
                    <div class="col-md-6">
                      <h3 class="heading">Mission</h3>
                      <p>To provide a quality and affordable spring resorts. Allow guest to experience the consistency
                        of relaxation that suits for them to unwind.
                        To give a good services and comfortable vacation. Recommendations is in favor to what people
                        expected to the resort.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-content" data-tab-content="tab2">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6">
                      <img src="images/image15.jpg" class="img-responsive" alt="Image">
                    </div>
                    <div class="col-md-6">
                      <h3 class="heading">Vision</h3>
                      <p>To be one of the well-known spring resorts in Calauan Laguna. To improve the facility,
                        services, and naturalness of the resort. Bring low end offers which guests can experience the
                        joy and hospitality of Malanday Mercedes Spring Resort. </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-content" data-tab-content="tab3">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6">
                      <img src="images/image9.jpg" class="img-responsive" alt="Image">
                    </div>
                    <div class="col-md-6">
                      <h3 class="heading">History</h3>
                      <p>Malanday Mercedes Spring Resort situated in the middle of the forest located at Barangay, Perez
                        Calauan Laguna the estimated size of the resort is four (4) hectares. The land was inherited
                        from the owner grandparents. It has small spring water, residents of Barangay Perez always
                        visited the small river until the father of the owner decided to create a small pool, but it was
                        destroyed from a typhoon. The owner and his father realized to renovate and enlarge the pool,
                        until they discovered the spring water has cold and hot temperature, so they came up to an idea
                        to build a spring resort. Malanday means going down to the mountain while Mercedes is surname of
                        the owner grandparents. They opened the resort during the pandemic so they decided to make it
                        private, and it will exclusive to those customers who reserve the entire resort.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="testimonial">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <h2>Happy Customer Says...</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="testimony">
                <blockquote>
                  &ldquo;Good for family bonding, team building and celebration..very safe because it is private..worth
                  ang paglalakad ng 10 to 15 minutes because you will see the beauty of the nature..Highly recommended
                  even during rainy season&rdquo;
                </blockquote>
                <p><cite>Mary Gracey</cite></p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="testimony">
                <blockquote>
                  &ldquo;Best to swim especially during the rainy season.. The weather is cold and the water is warm
                  plus the greeneries around, the BEST!&rdquo;
                </blockquote>
                <p><cite>Irenea Garcia</cite></p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="testimony">
                <blockquote>
                  &ldquo;natural hot springs with matching exotic plants and really a nice place for relaxations.
                  &rdquo;
                </blockquote>
                <p><cite>Jun Buiser</cite></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php include "./lib/footer.php"; ?>

    </div>
    <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->
  <?php include "./lib/script.php"; ?>
</body>
<script>
  $(document).ready(function() {
    var pathname = window.location.pathname;
    if (!pathname.includes('index')) {
      $(".nav-homepage").removeClass('active');
    }
  });
</script>

</html>