-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2021 at 02:54 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `malanday`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `aboutID` int(11) NOT NULL,
  `aboutCategory` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`aboutID`, `aboutCategory`, `content`) VALUES
(1, 'location', 'location Content'),
(2, 'vision', 'Vision Content'),
(3, 'mission', 'Mission Content'),
(4, 'history', 'History Content');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `imgID` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`imgID`, `img`, `category`) VALUES
(1, 'coldspring1.jpg', 'Cold Spring'),
(2, 'coldspring2.jpg', 'Cold Spring'),
(3, 'cottages1.jpeg', 'Cottages'),
(4, 'cottages2.jpeg', 'Cottages'),
(5, 'hotspring1.jpeg', 'Hot Spring'),
(6, 'hotspring2.jpg', 'Hot Spring'),
(7, 'roomatticangle1.jpeg', 'Rooms'),
(8, 'roomatticangle2.jpeg', 'Rooms'),
(9, 'roomdown1.jpeg', 'Rooms'),
(10, 'roomdown2.jpeg', 'Rooms'),
(11, 'roomdown3.jpeg', 'Rooms');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`id`, `name`, `email`, `message`, `createdDate`, `active`) VALUES
(1, 'User', 'test1@email.test.com', 'Hello World', '2021-10-28 14:22:15', 0),
(2, 'test', 'test1@email.test.com', 'Sample Message', '2021-10-28 13:52:23', 1),
(3, 'test', 'test1@email.test.com', 'Sample Message', '2021-10-28 13:52:47', 1),
(4, 'test', 'mcmarquinez01@gmail.com', 'asdasd', '2021-11-28 09:57:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `ID` int(32) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `ContactNumber` text NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Adults` int(30) NOT NULL,
  `Children` int(30) NOT NULL,
  `Checkindate` varchar(255) NOT NULL,
  `Checkoutdate` varchar(255) NOT NULL,
  `Schedule` varchar(255) NOT NULL,
  `Pax` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `reference_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`ID`, `Name`, `ContactNumber`, `Address`, `Email`, `Adults`, `Children`, `Checkindate`, `Checkoutdate`, `Schedule`, `Pax`, `status`, `price`, `reference_no`) VALUES
(91, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-11-29', '2021-11-30', 'Day (8am-5pm)', '5-10pax (6,000)', 'Completed', 6000, 1637769857),
(92, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-04', '2021-12-05', 'Day (8am-5pm)', '5-10pax (6,000)', 'Cancelled', 6000, 1637770042),
(93, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-07', '2021-12-08', 'Night (5pm-10am)', '5-10 pax', 'Cancelled', 6000, 1637771464),
(94, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-11', '2021-12-12', 'Day (8am-5pm)', '5-10 pax', 'Cancelled', 4000, 1637773106),
(95, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-11-30', '2021-12-01', 'Night (5pm-10am)', '5-10 pax', 'Completed', 6000, 1637773895),
(96, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-11', '2021-12-12', 'Day (8am-5pm)', '5-10 pax', 'Reserved', 4000, 1637802422),
(97, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-11-25', '2021-11-26', 'Day (8am-5pm)', '5-10 pax', 'Pending', 4000, 1637802906),
(98, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-11-28', '2021-11-29', 'Day (8am-5pm)', '5-10 pax', 'Pending', 4000, 1637803938),
(99, 'Mark Christian Castro Marquinez', '09651985796', 'Marcelo Ave', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-01', '2021-12-01', 'Day (8am-5pm)', '5-10 pax', 'Pending', 4000, 1638095885),
(100, 'Mark Christian Castro Marquinez', '09651985796', 'Marcelo Ave', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-01', '2021-12-01', 'Day (8am-5pm)', '5-10 pax', 'Reserved', 4000, 1638095894),
(101, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2022-01-09', '2022-01-09', 'Day (8am-5pm)', '5-10 pax', 'Reserved', 4000, 1638105139),
(102, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2022-01-15', '2022-01-15', 'Day (8am-5pm)', '5-10 pax', 'Cancelled', 4000, 1638105202),
(103, 'Mark Christian Marquinez', '09651985796', '312 Bolboc Tuy Batangas', 'mcmarquinez01@gmail.com', 1, 1, '2021-12-11', '2021-12-11', 'Day (8am-5pm)', '5-10 pax', 'Pending', 4000, 1638105773);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `reservation_id`, `date`) VALUES
(52, 91, '2021-11-29'),
(53, 91, '2021-11-30'),
(54, 92, '2021-12-04'),
(55, 92, '2021-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `registeredDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`, `img`, `role`, `status`, `registeredDate`) VALUES
(1, 'admin', '12345678', 'default.png', 'admin', 1, '2021-07-14 16:14:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`aboutID`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`imgID`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `aboutID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `imgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
