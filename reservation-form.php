<?php
if (isset($_GET['reservation'])) {
	if ($_GET['reservation'] == "success") {
		$query = "SELECT * FROM `reservation`WHERE `reference_no`=" . $_GET['ref'];
		$result = $conn->query($query);
		$row = $result->fetch_assoc();
		echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#invoiceModal").modal("show");
			});
		</script>';
	}
	if ($_GET['reservation'] == "error") {
		echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#errorModal").modal("show");
			});
		</script>';
	}
}
?>

<form method="post" id="reserveForm" action='query.php'>
  <div class="card card-primary card-outline">
    <div class="card-header">
      <h1 class="card-title">
        <i class="fas fa-edit"></i>
        Reservation Form
      </h1>
    </div>

    <div class="card-body row">

      <div class="col-12">

        <div class="form-group">
          <label>Name:</label>
          <input type="text" placeholder="Enter Full Name" name="Name" id="getName" class="form-control" required>
        </div>

        <div class="form-group">
          <label>Contact Number:</label>
          <input type="text" placeholder="Enter Contact Number" name="ContactNumber" id="getContactNumber"
            class="form-control" required>
        </div>

        <div class="form-group">
          <label>Address:</label>
          <input type="text" placeholder="Enter Address" name="Address" id="getAddress" class="form-control" required>
        </div>

        <div class="fillup">
          <label for name>Name:</label>
          <input type="text" placeholder="Input fullname" name="Name" id="getName" class="form-control" required>
        </div>

        <div class="form-group">
          <label>Email:</label>
          <input type="email" placeholder="Enter Email Address" name="Email" id="getEmail" class="form-control"
            required>
        </div>

        <div class="form-group">
          <label>Adults:</label>
          <input type="number" min="0" value="0" placeholder="Number of Adults" onchange="recalc()" name="Adults"
            id="getAdults" class="form-control" required>
        </div>

        <div class="form-group">
          <label>Children:</label>
          <input type="number" min="0" value="0" placeholder="Number of Children" onchange="recalc()" name="Children"
            id="getChildren" class="form-control" required>
        </div>

        <div class="form-group">
          <label>Checkin-date:</label>
          <input type="date" onchange="chckDateRange()" min="<?php echo date("Y-m-d") ?>" name="Checkindate"
            id="getCheckindate" class="form-control" required>
        </div>

        <div class="form-group">
          <label>Checkout-date:</label>
          <input type="date" name="Checkoutdate" id="getCheckoutdate" class="form-control" required disabled>
        </div>

        <div class="form-group">
          <label>Select Schedule</label>
          <select class="form-control" id="swimHour" name="SwimHour" required>
            <option value=""></option>
            <option value="Day (8am-5pm)">Day (8am-5pm)</option>
            <option value="Night (5pm-10am)">Night (5pm-10am)</option>
          </select>
        </div>
        <div class="form-group">
          <label>Pax</label>
          <select class="form-control" id="pax" name="Pax" required disabled>
            <option value=""></option>
            <option value="5-10pax (6,000)">5-10pax (₱6,000)</option>
            <option value="11-20pax (8,000)">11-20pax (₱8,000)</option>
            <option value="21-30pax (10,000)">21-30pax (₱10,000)</option>
          </select>
        </div>

        <div style="text-align:right;">
          <input type="button" value="Clear" onclick="reset()" class="btn btn-default">
          <!-- <input type="submit" value="SUBMIT" name="submit" class="btn btn-primary"> -->
          <input type="button" value="Submit" onclick="confirmForm()" class="btn btn-success">
        </div>
      </div>

      <input type="hidden" name="price" id="getprice">
      <input type="hidden" name="formpax" id="formpax">
      <input type="hidden" name="status" id="getgetstatus" value="Reserved">

    </div>
    <!-- Confirmation Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Confirm Reservation</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="callout callout-warning">
              <p><strong>Note: </strong> To continue your reservation you will have to pay ₱1000 for the reservation
                fee.</p>
            </div>
            <h5>CUSTOMER INFORMATION</h5>
            <table class="table">
              <tbody>
                <tr>
                  <th style="min-width:100px">Full Name:</th>
                  <td id="setName"></td>
                </tr>
                <tr>
                  <th>Email:</th>
                  <td id="setEmail"></td>
                </tr>
                <tr>
                  <th>Contact Number:</th>
                  <td id="setContact"></td>
                </tr>
                <tr>
                  <th>Address:</th>
                  <td id="setAddress"></td>
                </tr>
              </tbody>
            </table>
            <hr>
            <h5>RESERVATION DETAILS</h5>
            <table class="table">
              <tbody>
                <tr>
                  <th style="min-width:100px">Check-in Date</th>
                  <td id="setCheckin"></td>
                </tr>
                <tr>
                  <th>Check-out Date:</th>
                  <td id="setCheckout"></td>
                </tr>
                <tr>
                  <th>Schedule:</th>
                  <td id="setSchedule"></td>
                </tr>
                <tr>
                  <th>Pax:</th>
                  <td id="setPax"></td>
                </tr>
                <tr>
                  <th>Number of Adults:</th>
                  <td id="setAdultCount"></td>
                </tr>
                <tr>
                  <th>Number of Children:</th>
                  <td id="setChildrenCount"></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <!-- <button type="submit" value="submit" class="btn btn-success">Reserve</button> -->
            <input type="submit" value="Reserve" name="submit" class="btn btn-success">
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Invoice Modal -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="invoice">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Reservation Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="callout callout-success">
          <h5><strong>Success!</strong> Your reservation has been successfully scheduled.</h5>

          <p>The invoice will be sent to your email, please verify that your email is correct. If you wish to cancel
            your reservation. You may send us a message via SMS or email.</p>
          <p>Please note that reservation fees are non-refundable.</p>
        </div>
        <h5>CUSTOMER INFORMATION</h5>
        <table class="table">
          <tbody>
            <tr>
              <th style="min-width:100px">Full Name:</th>
              <td><?php echo $row['Name'] ?></td>
            </tr>
            <tr>
              <th>Email:</th>
              <td><?php echo $row['Email'] ?></td>
            </tr>
            <tr>
              <th>Contact Number:</th>
              <td><?php echo $row['ContactNumber'] ?></td>
            </tr>
            <tr>
              <th>Address:</th>
              <td><?php echo $row['Address'] ?></td>
            </tr>
          </tbody>
        </table>
        <hr>
        <h5>RESERVATION DETAILS</h5>
        <table class="table">
          <tbody>
            <tr>
              <th style="min-width:100px">Reference No.</th>
              <td id="refNo"><?php echo $row['reference_no'] ?></td>
            </tr>
            <tr>
              <th style="min-width:100px">Status</th>
              <td>
                <?php if ($row['status'] == "Reserved") { ?>
                <span class="badge badge-success">Reserved</span>
                <?php  } else { ?>
                <span class="badge badge-danger">Cancelled</span>
                <?php  } ?>
              </td>
            </tr>
            <tr>
              <th style="min-width:100px">Check-in Date</th>
              <td><?php echo date_format(date_create($row['Checkindate']), 'F j, Y') ?></td>
            </tr>
            <tr>
              <th>Check-out Date:</th>
              <td><?php echo date_format(date_create($row['Checkoutdate']), 'F j, Y') ?></td>
            </tr>
            <tr>
              <th>Schedule:</th>
              <td><?php echo $row['Schedule'] ?></td>
            </tr>
            <tr>
              <th>Pax:</th>
              <td><?php echo $row['Pax'] ?></td>
            </tr>
            <tr>
              <th>Number of Adults:</th>
              <td><?php echo $row['Adults'] ?></td>
            </tr>
            <tr>
              <th>Number of Children:</th>
              <td><?php echo $row['Children'] ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="error">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Reservation Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <h5><strong class="text-warning">Unavailable dates!</strong> Your reservation didn't get through.</h5>

        <p>Selected date/s is/are not available. Please check resort's availability <a
            href="index.php?availability">here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  // $('#invoiceModal').modal('show');
  $("#invoiceModal").on("hidden.bs.modal", function() {
    location.replace("/malanday/index.php?availability");
  });
})

function confirmForm() {
  //Confirm reservation details
  if (validateForm()) {
    $('#formpax').val($('#pax').val());

    // Customer Details
    $('#setName').html($('#getName').val());
    $('#setContact').html($('#getContactNumber').val());
    $('#setEmail').html($('#getEmail').val());
    $('#setAddress').html($('#getAddress').val());

    // Reservation Details
    $('#setCheckin').html(dateToString($('#getCheckindate').val()));
    $('#setCheckout').html(dateToString($('#getCheckoutdate').val()));
    $('#setSchedule').html($('#swimHour').val());
    $('#setPax').html($('#pax').val());
    $('#setAdultCount').html($('#getAdults').val());
    $('#setChildrenCount').html($('#getChildren').val());

    $('#myModal').modal('show');
  }

  // console.log(getName);
  // $('#myModal').modal('show');
}

function dateToString(date) {
  //Format Date
  var month = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]
  d = new Date(date);
  var getMonth = d.getMonth();
  var stringDate = month[getMonth] + " " + d.getDate() + ", " + d.getFullYear();
  return stringDate;
}


function validateForm() {
  //Validate Form
  if (document.forms["reserveForm"]["Name"].value == "") {
    document.forms["reserveForm"]["Name"].classList.add('is-invalid');
    document.forms["reserveForm"]["Name"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["Name"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["ContactNumber"].value == "") {
    document.forms["reserveForm"]["ContactNumber"].classList.add('is-invalid');
    document.forms["reserveForm"]["ContactNumber"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["ContactNumber"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getAddress"].value == "") {
    document.forms["reserveForm"]["getAddress"].classList.add('is-invalid');
    document.forms["reserveForm"]["getAddress"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getAddress"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getEmail"].value == "") {
    document.forms["reserveForm"]["getEmail"].classList.add('is-invalid');
    document.forms["reserveForm"]["getEmail"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getEmail"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getAdults"].value == "") {
    document.forms["reserveForm"]["getAdults"].classList.add('is-invalid');
    document.forms["reserveForm"]["getAdults"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getAdults"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getChildren"].value == "") {
    document.forms["reserveForm"]["getChildren"].classList.add('is-invalid');
    document.forms["reserveForm"]["getChildren"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getChildren"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getCheckindate"].value == "") {
    document.forms["reserveForm"]["getCheckindate"].classList.add('is-invalid');
    document.forms["reserveForm"]["getCheckindate"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getCheckindate"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getCheckoutdate"].value == "") {
    document.forms["reserveForm"]["getCheckoutdate"].classList.add('is-invalid');
    document.forms["reserveForm"]["getCheckoutdate"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getCheckoutdate"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["getCheckoutdate"].value == "") {
    document.forms["reserveForm"]["getCheckoutdate"].classList.add('is-invalid');
    document.forms["reserveForm"]["getCheckoutdate"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["getCheckoutdate"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["swimHour"].value == "") {
    document.forms["reserveForm"]["swimHour"].classList.add('is-invalid');
    document.forms["reserveForm"]["swimHour"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["swimHour"].classList.remove('is-invalid');
  }
  if (document.forms["reserveForm"]["pax"].value == "") {
    document.forms["reserveForm"]["pax"].classList.add('is-invalid');
    document.forms["reserveForm"]["pax"].focus();
    return false;
  } else {
    document.forms["reserveForm"]["pax"].classList.remove('is-invalid');
  }

  return true;
}

function recalc() {
  //Recalculate Pax by adjusting adult and children input
  var adltCnt = $('#getAdults').val();
  var chldCnt = $('#getChildren').val();
  var total = parseInt(adltCnt) + parseInt(chldCnt);

  if (total < 11) {
    $('#pax').val("5-10pax (6,000)").change();
    $('#getprice').val(6000);
  }

  if (total > 10 && total < 21) {
    $('#pax').val("11-20pax (8,000)").change();
    $('#getprice').val(8000);
  }

  if (total > 20 && total < 31) {
    $('#pax').val("21-30pax (10,000)").change();
    $('#getprice').val(10000);
  }

  if (total > 30) {
    $('#pax').val("");
    $('#getprice').val("");
    $('#getAdults').val("");
    $('#getChildren').val("");
    alert("Total pax per reservation can't be more than 30");
  }
}

function chckDateRange() {
  //Form validation for checkout date
  if ($('#getCheckindate').val() != "") {
    $('#getCheckoutdate').attr("min", $('#getCheckindate').val());
    $('#getCheckoutdate').attr("disabled", false);
  } else {
    $('#getCheckoutdate').attr("disabled", true);
    $('#getCheckoutdate').val("");
  }
}

function successReserve() {

}
</script>