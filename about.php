<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<?php include "./lib/head.php"; ?>

<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">
      <?php include "./lib/navbar.php"; ?>
      <!-- end:fh5co-header -->
      <div class="fh5co-parallax" style="background-image: url(images/slider1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
              <div class="fh5co-intro fh5co-table-cell">
                <h1 class="text-center">About Us</h1>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div id="fh5co-services-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="services">
                <span><i class="ti-file"></i></span>
                <div class="desc">
                  <h3>History</h3>
                  <p>Malanday Mercedes Spring Resort situated in the middle of the forest located at Barangay, Perez
                    Calauan Laguna the estimated size of the resort is four (4) hectares. The land was inherited from
                    the owner grandparents. It has small spring water, residents of Barangay Perez always visited the
                    small river until the father of the owner decided to create a small pool, but it was destroyed from
                    a typhoon. The owner and his father realized to renovate and enlarge the pool, until they discovered
                    the spring water has cold and hot temperature, so they came up to an idea to build a spring resort.
                    Malanday means going down to the mountain while Mercedes is surname of the owner grandparents. They
                    opened the resort during the pandemic so they decided to make it private, and it will exclusive to
                    those customers who reserve the entire resort.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="services">
                <span><i class="ti-target"></i></span>
                <div class="desc">
                  <h3>Misson</h3>
                  <p>To provide a quality and affordable spring resorts. Allow guest to experience the consistency of
                    relaxation that suits for them to unwind. To give a good services and comfortable vacation.
                    Recommendations is in favor to what people expected to the resort.</p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="services">
                <span><i class="ti-eye"></i></span>
                <div class="desc">
                  <h3>Vision</h3>
                  <p>To be one of the well-known spring resorts in Calauan Laguna. To improve the facility, services,
                    and naturalness of the resort. Bring low end offers which guests can experience the joy and
                    hospitality of Malanday Mercedes Spring Resort. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php include "./lib/footer.php"; ?>

    </div>
    <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->

  <?php include "./lib/script.php"; ?>

</body>

<script>
  $(document).ready(function() {
    var pathname = window.location.pathname;
    if (pathname.includes('about')) {
      $(".nav-about").addClass('active');

      $(".nav-homepage").removeClass('active');
      $(".nav-gallery").removeClass('active');
      $(".nav-contact").removeClass('active');
      $(".nav-reservation").removeClass('active');
    }
  });
</script>

</html>