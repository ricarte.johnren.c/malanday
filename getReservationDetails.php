<?php
require_once 'connection/connect.php';
date_default_timezone_set("Asia/Manila"); //Timezone

$refno = $_REQUEST["refno"];
$getDetails = mysqli_query($conn, "SELECT * FROM `reservation` WHERE `reference_no` = '$refno'");
$result = mysqli_fetch_assoc($getDetails);

$reservationData = new stdClass();
$reservationData->name = $result['Name'];
$reservationData->contact = $result['ContactNumber'];
$reservationData->address = $result['Address'];
$reservationData->email = $result['Email'];
$reservationData->checkin = date_format(date_create($result['Checkindate']), 'F j, Y');
$reservationData->checkout = date_format(date_create($result['Checkoutdate']), 'F j, Y');
$reservationData->refno = $result['reference_no'];
$reservationData->schedule = $result['Schedule'];
$reservationData->pax = $result['Pax'];
$reservationData->adult = $result['Adults'];
$reservationData->child = $result['Children'];
$reservationData->resortFee = "₱ ".number_format($result['price'],2);
$reservationData->resFee = "₱ 1000.00";
$reservationData->conFee =  "₱ 25.00";
$reservationData->status = $result['status'];
$reservationData->ttlFee = "₱ ".number_format($result['price']+25,2);
if($result['status'] == "Completed"){
    $reservationData->balance = "₱ 0.00";
}else{
    $reservationData->balance = "₱ ".number_format(($result['price'] - 1000),2);
}

$data = json_encode($reservationData);

echo $data;
?>
