<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Malanday | Resort</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="index2.html" class="h1"><b>Malanday|</b>Resort</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Sign in your Admin Account</p>

                <form action="logcheck.php" method="post">
                    <div class="input-group mb-3">
                        <input type="text" name='username' required class="form-control" placeholder="Username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name='password' required class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">



                        <div class="col-12">
                            <div class='text-red col-xs-12' style="text-align:center;">
                                <?php
		if(isset($_GET['error_user']))
		{
		
		echo "Username does not exist.!";
		echo "<br>";
		echo "<br>";
		}
		else if(isset($_GET['error_pass']))
		{
		
		echo "Incorrect Password.!";
		echo "<br>";
		echo "<br>";
		}
		else if(isset($_GET['error_stat']))
		{
		
		echo "Account Locked.!";
		echo "<br>";
		echo "<br>";
		}
		?>
                            </div>
                            <button type="submit" name='log' pull-right class="btn btn-primary btn-block">Sign
                                In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>




            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>