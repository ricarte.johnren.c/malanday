<?php include "./lib/script.php"; ?>
<?php
require_once './connection/connect.php';

if (isset($_GET['reservation'])) {
  if ($_GET['reservation'] == "success") {
    $query = "SELECT * FROM `reservation`WHERE `reference_no`=" . $_GET['ref'];
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#invoiceModal").modal("show");
			});
		</script>';
  }
  if ($_GET['reservation'] == "error") {
    echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#errorModal").modal("show");
			});
		</script>';
  }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<?php include "./lib/head.php"; ?>

<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">
      <?php include "./lib/navbar.php"; ?>
      <!-- end:fh5co-header -->
      <div class="fh5co-parallax" style="background-image: url(images/slider1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
              <div class="fh5co-intro fh5co-table-cell">
                <h1 class="text-center">Reserve slot for vacation</h1>
                <p>A Best Place To Enjoy Your Life</p>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div id="fh5co-services-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="services">

                <div class="desc">
                  <p class="card-text">Please fill-in all the information. </p>
                  <form method="post" id="reserveForm" action='./payment/create-source.php'>
                    <label>Client:</label>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" placeholder="Name" name="Name" id="getName" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="email" placeholder="Enter Email Address" name="Email" id="getEmail" class="form-control" required>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="number" placeholder="Contact Number" name="ContactNumber" id="getContactNumber" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" placeholder="Address" name="Address" id="getAddress" class="form-control" required>
                        </div>
                      </div>
                    </div>

                    <label>Reservation Details:</label>

                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                          <input type="number" min="0" placeholder="Number of Adults" onchange="recalc()" name="Adults" id="getAdults" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <input type="number" min="0" placeholder="Number of Children" onchange="recalc()" name="Children" id="getChildren" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <select class="form-control" id="pax" name="Pax" required disabled>
                            <option value="">pax</option>
                            <option value="5-10 pax">5-10 pax</option>
                            <option value="11-20 pax">11-20 pax</option>
                            <option value="21-30 pax">21-30 pax</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" placeholder="Check-in Date" onblur="(this.type='text')" onfocus="(this.type='date')" onchange="chckDateRange()" min="<?php echo date("Y-m-d") ?>" name="Checkindate" id="getCheckindate" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" placeholder="Check-out Date" onblur="(this.type='text')" onfocus="(this.type='date')" name="Checkoutdate" id="getCheckoutdate" class="form-control" required disabled>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <select class="form-control" id="swimHour" name="SwimHour" onchange="recalc()" required>
                            <option value="">Select Schedule</option>
                            <option value="Day (8am-5pm)">Day (8am-5pm)</option>
                            <option value="Night (5pm-10am)">Night (5pm-10am)</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-12">
                      <div style="text-align:right;">
                        <input type="button" value="Clear" onclick="reset()" class="btn btn-default">
                        <!-- <input type="submit" value="SUBMIT" name="submit" class="btn btn-primary"> -->
                        <input type="button" value="Submit" onclick="confirmForm()" class="btn btn-success">
                      </div>
                    </div>
                    <input type="hidden" name="price" id="getprice">
                    <input type="hidden" name="formpax" id="formpax">
                    <input type="hidden" name="status" id="getgetstatus" value="Reserved">

                    <!-- Confirmation Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Confirm
                              Reservation</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span></button>
                          </div>
                          <div class="modal-body">
                            <div class="callout callout-warning">
                              <p><strong>Note: </strong> To continue your reservation you will have to pay ₱1000 for the
                                reservation fee.</p>
                            </div>
                            <h5>CUSTOMER INFORMATION</h5>
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:300px">Full Name:</th>
                                  <td id="setName"></td>
                                </tr>
                                <tr>
                                  <th>Email:</th>
                                  <td id="setEmail"></td>
                                </tr>
                                <tr>
                                  <th>Contact Number:</th>
                                  <td id="setContact"></td>
                                </tr>
                                <tr>
                                  <th>Address:</th>
                                  <td id="setAddress"></td>
                                </tr>
                              </tbody>
                            </table>
                            <hr>
                            <h5>RESERVATION DETAILS</h5>
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:300px">Check-in Date</th>
                                  <td id="setCheckin"></td>
                                </tr>
                                <tr>
                                  <th>Check-out Date:</th>
                                  <td id="setCheckout"></td>
                                </tr>
                                <tr>
                                  <th>Schedule:</th>
                                  <td id="setSchedule"></td>
                                </tr>
                                <tr>
                                  <th>Pax:</th>
                                  <td id="setPax"></td>
                                </tr>
                                <tr>
                                  <th>Number of Adults:</th>
                                  <td id="setAdultCount"></td>
                                </tr>
                                <tr>
                                  <th>Number of Children:</th>
                                  <td id="setChildrenCount"></td>
                                </tr>
                              </tbody>
                            </table>
                            <h5>PRICING</h5>
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:300px">Resort Fee:</th>
                                  <td id="setResortFee"></td>
                                </tr>
                                <tr>
                                  <th>Reservation Fee:</th>
                                  <td id="setResFee"></td>
                                </tr>
                                <tr>
                                  <th>Convenience Fee:</th>
                                  <td id="setConFee"></td>
                                </tr>
                                <tr>
                                  <th>Balance:</th>
                                  <td id="setBalance"></td>
                                </tr>
                                <tr>
                                  <td><b>Total Balance</b> (Balance + Convenience Fee):</td>
                                  <th id="setTotalFee"></th>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <!-- <button type="submit" value="submit" class="btn btn-success">Reserve</button> -->
                            <input type="submit" value="Reserve" name="submit" class="btn btn-success">
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Confirmation Modal -->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php include "./lib/footer.php"; ?>

    </div>
    <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->

  <!-- Invoice Modal -->
  <div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="invoice">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Reservation Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <div class="callout callout-success">
            <h5><strong>Success!</strong> Your reservation has been successfully scheduled.</h5>

            <p>The invoice will be sent to your email, please verify that your email is correct. If you wish to cancel
              your reservation. You may send us a message via SMS or email.</p>
            <p>Please note that reservation fees are non-refundable.</p>
          </div>
          <h5>CUSTOMER INFORMATION</h5>
          <table class="table">
            <tbody>
              <tr>
                <th style="min-width:100px">Full Name:</th>
                <td><?php echo $row['Name'] ?></td>
              </tr>
              <tr>
                <th>Email:</th>
                <td><?php echo $row['Email'] ?></td>
              </tr>
              <tr>
                <th>Contact Number:</th>
                <td><?php echo $row['ContactNumber'] ?></td>
              </tr>
              <tr>
                <th>Address:</th>
                <td><?php echo $row['Address'] ?></td>
              </tr>
            </tbody>
          </table>
          <hr>
          <h5>RESERVATION DETAILS</h5>
          <table class="table">
            <tbody>
              <tr>
                <th style="min-width:100px">Reference No.</th>
                <td id="refNo"><?php echo $row['reference_no'] ?></td>
              </tr>
              <tr>
                <th style="min-width:100px">Status</th>
                <td>
                  <?php if ($row['status'] == "Reserved") { ?>
                    <span class="badge badge-success">Reserved</span>
                  <?php  } else { ?>
                    <span class="badge badge-danger">Cancelled</span>
                  <?php  } ?>
                </td>
              </tr>
              <tr>
                <th style="min-width:100px">Check-in Date</th>
                <td><?php echo date_format(date_create($row['Checkindate']), 'F j, Y') ?></td>
              </tr>
              <tr>
                <th>Check-out Date:</th>
                <td><?php echo date_format(date_create($row['Checkoutdate']), 'F j, Y') ?></td>
              </tr>
              <tr>
                <th>Schedule:</th>
                <td><?php echo $row['Schedule'] ?></td>
              </tr>
              <tr>
                <th>Pax:</th>
                <td><?php echo $row['Pax'] ?></td>
              </tr>
              <tr>
                <th>Number of Adults:</th>
                <td><?php echo $row['Adults'] ?></td>
              </tr>
              <tr>
                <th>Number of Children:</th>
                <td><?php echo $row['Children'] ?></td>
              </tr>
            </tbody>
          </table>
          <h5>PRICING</h5>
          <table class="table">
            <tbody>
              <tr>
                <th style="width:300px">Resort Fee:</th>
                <td><?php echo "₱ " . number_format($row['price']) . ".00" ?></td>
              </tr>
              <tr>
                <th>Reservation Fee:</th>
                <td><?php echo "₱ 1,000.00" ?></td>
              </tr>
              <tr>
                <th>Convenience Fee:</th>
                <td><?php echo "₱ 25.00" ?></td>
              </tr>
              <tr>
                <th>Balance:</th>
                <td><?php echo "₱ " . number_format($row['price'] - 1000) . ".00" ?></td>
              </tr>
              <tr>
                <td><b>Total Balance</b> (Balance + Convenience Fee):</td>
                <th><?php echo "₱ " . number_format(($row['price'] - 1000) + 25) . ".00" ?></th>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Reservation Error Modal -->
  <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="error">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-exclamation-circle info"></span> Reservation Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <h5><strong class="text-warning">Something went wrong!</strong></h5>

          <p><?php echo $_GET['msg'] ?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



  <script>
    $(document).ready(function() {
      $('#invoiceModal').on('hidden.bs.modal', function() {
        window.location.replace("index.php");
      })
    });

    function recalc() {
      //Recalculate Pax by adjusting adult and children input
      var adltCnt = $('#getAdults').val();
      var chldCnt = $('#getChildren').val();
      var schedule = $('#swimHour').val();
      var total = parseInt(adltCnt) + parseInt(chldCnt);

      if (total < 11) {
        $('#pax').val("5-10 pax").change();
        $('#getprice').val(6000);
      }

      if (total > 10 && total < 21) {
        $('#pax').val("11-20 pax").change();
        $('#getprice').val(8000);
      }

      if (total > 20 && total < 31) {
        $('#pax').val("21-30 pax").change();
        $('#getprice').val(10000);
      }

      if (total > 30) {
        $('#pax').val("");
        $('#getprice').val("");
        $('#getAdults').val("");
        $('#getChildren').val("");
        alert("Total pax per reservation can't be more than 30");
      }

      if (schedule == "Day (8am-5pm)") {
        $('#getprice').val($('#getprice').val() - 2000);
      }

      // Pricing Details
      var rsrtFee = $('#getprice').val();
      var resFee = 1000;
      var bal = $('#getprice').val() - 1000;
      var conFee = 25;

      $('#setResortFee').html(formatNum(rsrtFee));
      $('#setResFee').html(formatNum(resFee));
      $('#setConFee').html(formatNum(conFee));
      $('#setBalance').html(formatNum(bal));
      $('#setTotalFee').html(formatNum(bal + conFee));
    }

    function formatNum(x) {
      var formatted = "₱ " + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00"
      return formatted;
    }

    function chckDateRange() {
      //Form validation for checkout date
      if ($('#getCheckindate').val() != "") {
        $('#getCheckoutdate').attr("min", $('#getCheckindate').val());
        $('#getCheckoutdate').attr("disabled", false);
      } else {
        $('#getCheckoutdate').attr("disabled", true);
        $('#getCheckoutdate').val("");
      }
    }

    function confirmForm() {
      //Confirm reservation details
      if (validateForm()) {
        $('#formpax').val($('#pax').val());

        // Customer Details
        $('#setName').html($('#getName').val());
        $('#setContact').html($('#getContactNumber').val());
        $('#setEmail').html($('#getEmail').val());
        $('#setAddress').html($('#getAddress').val());

        // Reservation Details
        $('#setCheckin').html(dateToString($('#getCheckindate').val()));
        $('#setCheckout').html(dateToString($('#getCheckoutdate').val()));
        $('#setSchedule').html($('#swimHour').val());
        $('#setPax').html($('#pax').val());
        $('#setAdultCount').html($('#getAdults').val());
        $('#setChildrenCount').html($('#getChildren').val());

        $('#myModal').modal('show');
      }

      // console.log(getName);
      // $('#myModal').modal('show');
    }

    function validateForm() {
      //Validate Form
      if (document.forms["reserveForm"]["Name"].value == "") {
        document.forms["reserveForm"]["Name"].classList.add('is-invalid');
        document.forms["reserveForm"]["Name"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["Name"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["ContactNumber"].value == "") {
        document.forms["reserveForm"]["ContactNumber"].classList.add('is-invalid');
        document.forms["reserveForm"]["ContactNumber"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["ContactNumber"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getAddress"].value == "") {
        document.forms["reserveForm"]["getAddress"].classList.add('is-invalid');
        document.forms["reserveForm"]["getAddress"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getAddress"].classList.remove('is-invalid');
      }

      var emailRegexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
      var emailValue = document.forms["reserveForm"]["getEmail"].value;
      var isMatch = emailRegexp.test(emailValue);

      if (emailValue == "" || !emailRegexp.test(emailValue)) {
        document.forms["reserveForm"]["getEmail"].classList.add('is-invalid');
        document.forms["reserveForm"]["getEmail"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getEmail"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getAdults"].value == "") {
        document.forms["reserveForm"]["getAdults"].classList.add('is-invalid');
        document.forms["reserveForm"]["getAdults"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getAdults"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getChildren"].value == "") {
        document.forms["reserveForm"]["getChildren"].classList.add('is-invalid');
        document.forms["reserveForm"]["getChildren"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getChildren"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getCheckindate"].value == "") {
        document.forms["reserveForm"]["getCheckindate"].classList.add('is-invalid');
        document.forms["reserveForm"]["getCheckindate"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getCheckindate"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getCheckoutdate"].value == "") {
        document.forms["reserveForm"]["getCheckoutdate"].classList.add('is-invalid');
        document.forms["reserveForm"]["getCheckoutdate"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getCheckoutdate"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["getCheckoutdate"].value == "") {
        document.forms["reserveForm"]["getCheckoutdate"].classList.add('is-invalid');
        document.forms["reserveForm"]["getCheckoutdate"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["getCheckoutdate"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["swimHour"].value == "") {
        document.forms["reserveForm"]["swimHour"].classList.add('is-invalid');
        document.forms["reserveForm"]["swimHour"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["swimHour"].classList.remove('is-invalid');
      }
      if (document.forms["reserveForm"]["pax"].value == "") {
        document.forms["reserveForm"]["pax"].classList.add('is-invalid');
        document.forms["reserveForm"]["pax"].focus();
        return false;
      } else {
        document.forms["reserveForm"]["pax"].classList.remove('is-invalid');
      }

      return true;
    }

    function dateToString(date) {
      //Format Date
      var month = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ]
      d = new Date(date);
      var getMonth = d.getMonth();
      var stringDate = month[getMonth] + " " + d.getDate() + ", " + d.getFullYear();
      return stringDate;
    }
  </script>
</body>

<script>
  $(document).ready(function() {
    var pathname = window.location.pathname;
    if (pathname.includes('reservation')) {
      $(".nav-reservation").addClass('active');

      $(".nav-homepage").removeClass('active');
      $(".nav-gallery").removeClass('active');
      $(".nav-contact").removeClass('active');
      $(".nav-about").removeClass('active');
    }
  });
</script>

</html>