<?php
include './auth.php';
require_once '../connection/connect.php'; 

if (isset($_POST['submit'])) {
  //Set values to session
  $_SESSION['res_name'] = $_POST['Name'];
  $_SESSION['res_contact'] = $_POST['ContactNumber'];
  $_SESSION['res_add'] = $_POST['Address'];
  $_SESSION['res_mail'] = $_POST['Email'];
  $_SESSION['res_adlts'] = $_POST['Adults'];
  $_SESSION['res_child'] = $_POST['Children'];
  $_SESSION['res_checkin'] = $_POST['Checkindate'];
  $_SESSION['res_checkout'] = $_POST['Checkoutdate'];
  $_SESSION['res_sched'] = $_POST['SwimHour'];
  $_SESSION['res_pax'] = $_POST['formpax'];
  $_SESSION['res_price'] = $_POST['price'];
  $_SESSION['res_status'] = $_POST['status'];
  $_SESSION['res_refno'] = time(); //timestamp for reference number

  //Checking availabilty of the schedules
  $chckAvailabilty = mysqli_query($conn, "SELECT * FROM `schedules` WHERE `date` BETWEEN '{$_POST['Checkindate']}' AND '{$_POST['Checkoutdate']}' AND `sched` = '{$_POST['SwimHour']}'");
  $availCount = $chckAvailabilty->num_rows;

  if ($availCount > 0) {
      header("Location: /malanday/reservation.php?reservation=error&msg=selected dates are not available. Please see calendar for the available schedules");
      exit();
  }

}else{
  header("Location: /malanday/reservation.php?reservation=error&msg=Unable to proceed, sorry for the inconvenice. Please see our contact page");

}

$curl = curl_init();
$host = "http://localhost";
$redirect_success = $host."/malanday/payment/create-payment.php";
$redirect_failed = $host."/malanday/reservation.php?reservation=error&msg=Unable to proceed, sorry for the inconvenice. Please see our contact page";

$amount = 100000; // this includes the decimal points and equivalent to 1000.00
$type = "gcash";

curl_setopt_array($curl, [
  CURLOPT_URL => "https://api.paymongo.com/v1/sources",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"data\":{\"attributes\":{\"amount\":$amount,\"redirect\":{\"success\":\"$redirect_success\",\"failed\":\"$redirect_failed\"},\"type\":\"$type\",\"currency\":\"PHP\"}}}",
  CURLOPT_HTTPHEADER => [
    "Accept: application/json",
    "Authorization: Basic $secret_key_base64",
    "Content-Type: application/json"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

$result = json_decode($response, true);
$source_data = $result['data'];

$_SESSION['src_id'] = $source_data['id'];

$redirect = $result['data']['attributes']['redirect'];

$checkout_url = $redirect['checkout_url'];

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  // we can directly open this checkout url to make the source chargeable

  header("Location:".$checkout_url);
  // echo $checkout_url;

}
