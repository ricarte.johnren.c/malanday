<?php
require_once '../connection/connect.php'; 
require_once '../admin/invoice/mailer.php'; 
include './auth.php';

$source_id = $_SESSION['src_id'];

$description = "For reservation purpose";
$statement_descrptor = "test payment";
$amount = 100000;
$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "https://api.paymongo.com/v1/payments",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"data\":{\"attributes\":{\"amount\":$amount,\"source\":{\"id\":\"$source_id\" ,\"type\":\"source\"},\"currency\":\"PHP\",\"statement_descriptor\":\"$statement_descrptor\",\"description\":\"$description\"}}}",
  CURLOPT_HTTPHEADER => [
    "Accept: application/json",
    "Authorization: Basic $secret_key_base64",
    "Content-Type: application/json"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  
  //GET DATA FROM SESSION
  $name = $_SESSION['res_name'];
  $contact = $_SESSION['res_contact'];
  $add = $_SESSION['res_add'];
  $mail = $_SESSION['res_mail'];
  $adlts = $_SESSION['res_adlts'];
  $child = $_SESSION['res_child'];
  $checkin = $_SESSION['res_checkin'];
  $checkout = $_SESSION['res_checkout'];
  $sched = $_SESSION['res_sched'];
  $pax = $_SESSION['res_pax'];
  $price = $_SESSION['res_price'];
  $status = $_SESSION['res_status'];
  $refno = $_SESSION['res_refno'];

  //Inserting records to reservation table
  $query = "INSERT INTO `reservation` (`ID`, `Name`, `ContactNumber`, `Address`, `Email`, `Adults`, `Children`, `Checkindate`, `Checkoutdate`, `Schedule`, `Pax`, `status`, `price`, `reference_no`) 
	VALUES (NULL, '{$name}', '{$contact}', '{$add}', '{$mail}', '{$adlts}', '{$child}', '{$checkin}', '{$checkout}', '{$sched}', '{$pax}', '{$status}', '{$price}', '{$refno}');";
  if ($result = $conn->query($query)) {
      //Get ID
      $rQuery = mysqli_query($conn, "SELECT * FROM `reservation` WHERE `reference_no`=" . $refno);
      $reservation = mysqli_fetch_array($rQuery);

      //Save schedules
      if ($checkin != $checkout) {
          $dates = getDates($checkin, $checkout);
          foreach ($dates as $key => $value) {
              $scheduleQuery = "INSERT INTO `schedules` VALUES (NULL, '{$reservation['ID']}', '{$value->format('Y-m-d')}', '{$sched}')";
              $insertSchedules = $conn->query($scheduleQuery);
          }
      } else {
          $scheduleQuery = "INSERT INTO `schedules` VALUES (NULL, '{$reservation['ID']}', '{$checkin}', '{$sched}')";
          $insertSchedules = $conn->query($scheduleQuery);
      }
  }

  sendMail($refno, $mail);

  session_destroy();

  header ("Location: ../reservation.php?reservation=success&ref=".$refno);
  echo $response;

}


function getDates($start, $end)
{
    //For getting the dates between the start date and end date
    $period = new DatePeriod(
        new DateTime($start),
        new DateInterval('P1D'),
        new DateTime(date('y-m-d', strtotime($end . ' +1 day')))
    );

    return $period;
}