<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<?php include "./lib/head.php"; ?>

<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">
      <?php include "./lib/navbar.php"; ?>
      <!-- end:fh5co-header -->
      <div class="fh5co-parallax" style="background-image: url(images/slider1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
              <div class="fh5co-intro fh5co-table-cell">
                <h1 class="text-center">Choose Our Resort</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="fh5co-hotel-section">
        <!-- <div class="container"> -->
        <div class="row">
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/room8.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Room 1</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image22.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Room 2</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/room9.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Room 3</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/room6.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Room 4</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/room7.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Room 5</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image4.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Relaxing Pool</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image12.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Relaxing Pool</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image9.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Relaxing Pool</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/pool-1.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Day Swimming</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image16.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Night Swimming</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/kiddie-pool-1.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Kiddie pool <small>(2ft. Warm water)</small></a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/kiddie-pool-2.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Kiddie pool <small>(5ft. Cold water)</small></a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/kiddie-pool-3.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Kiddie pool</a></h3>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image11.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Table</a></h3>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/image23.jpg);">
              </div>
              <div class="desc">
                <h3><a href="#">Kitchen</a></h3>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/cr-1.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Comfort Room</a></h3>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/cr-2.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">Comfort Room</a></h3>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <div class="hotel-content">
              <div class="hotel-grid" style="background-image: url(images/horse-1.jpeg);">
              </div>
              <div class="desc">
                <h3><a href="#">10-15 mins. Horse Ride</a></h3>
              </div>
            </div>
          </div>
        </div>
        <!-- </div> -->
      </div>


      <?php include "./lib/footer.php"; ?>
    </div>
    <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->

  <?php include "./lib/script.php"; ?>

</body>

<script>
  $(document).ready(function() {
    var pathname = window.location.pathname;
    if (pathname.includes('gallery')) {
      $(".nav-gallery").addClass('active');

      $(".nav-homepage").removeClass('active');
      $(".nav-reservation").removeClass('active');
      $(".nav-contact").removeClass('active');
      $(".nav-about").removeClass('active');
    }
  });
</script>

</html>